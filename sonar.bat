SonarQube.Scanner.MSBuild.exe begin /k:ExemploPipeline /n:ExemploPipeline /v:1.0 /d:sonar.host.url=%SONAR_URL% /d:sonar.sources=ExemploPipeline /d:sonar.verbose=true
dotnet restore
dotnet msbuild /t:Rebuild
SonarQube.Scanner.MSBuild.exe end